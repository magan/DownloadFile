﻿Imports System.Net

Module ModuleMain

    Private Enum ExitCodes
        OK = 0
        SETTINGS = 1
        CANCELLED = 2
        [ERROR] = 3
    End Enum

    Private Source As String
    Private Destination As String

    Private CursorLeft As Integer
    Private CursorTop As Integer

    Private WithEvents Client As WebClient

    Sub Main()

        Select Case My.Application.CommandLineArgs.Count
            Case 1
                ' Arg1 = Source URI, destination is current working directory with original filename
                Source = My.Application.CommandLineArgs(0)
                Destination = IO.Path.GetFileName(Source)

            Case 2
                ' Arg1 = Source URI
                ' Arg2 = Destination path, with or without filename
                Source = My.Application.CommandLineArgs(0)
                Destination = My.Application.CommandLineArgs(1)

                If IO.Path.GetFileName(Destination) = "" Then
                    ' Destination is a folder
                    If Not IO.Directory.Exists(Destination) Then IO.Directory.CreateDirectory(Destination)
                    Destination = IO.Path.Combine(Destination, IO.Path.GetFileName(Source))
                Else
                    ' Destination is a file
                End If

            Case Else
                ' Invalid arguments
                Console.WriteLine("Usage: " & My.Application.Info.AssemblyName & " <url>")
                Environment.Exit(ExitCodes.SETTINGS)

        End Select

        ' Make sure the destination file doesn't exist
        If IO.File.Exists(Destination) Then IO.File.Delete(Destination)

        ' Make sure we can handle ftps:// style URL:s (FTP over SSL)
        WebRequest.RegisterPrefix("ftps", New FtpsWebRequestCreator())

        ' Accept insecure SSL certificates
        ServicePointManager.ServerCertificateValidationCallback = New Security.RemoteCertificateValidationCallback(
            Function()
                Return True
            End Function
            )

        ' Save current location
        CursorLeft = Console.CursorLeft
        CursorTop = Console.CursorTop

        ' Download asynchronous (to get progress reports)
        Client = New WebClient()
        Client.DownloadFileAsync(New Uri(Source), Destination)

        ' Sleep until download is complete
        Threading.Thread.Sleep(System.Threading.Timeout.Infinite)

    End Sub

    Private Function FormatSize(Value As Long) As String
        Dim dValue As Double = Value
        Dim Unit As String = "byte(s)"
        Dim sFormat As String = "#,##0" ' No decimals on bytes

        If dValue > 1024 Then
            dValue = dValue / 1024
            Unit = "KiB"
            sFormat = "#,##0.00" ' Use decimals on everything else
        End If

        If dValue > 1024 Then
            dValue = dValue / 1024
            Unit = "MiB"
        End If

        If dValue > 1024 Then
            dValue = dValue / 1024
            Unit = "GiB"
        End If

        Return Format(dValue, sFormat) & " " & Unit
    End Function

    Private Sub Client_DownloadFileCompleted(sender As Object, e As ComponentModel.AsyncCompletedEventArgs) Handles Client.DownloadFileCompleted
        If e.Cancelled Then
            Console.WriteLine("Download cancelled")
            Environment.Exit(ExitCodes.CANCELLED)
        ElseIf Not e.Error Is Nothing Then
            ' File not found, network error, etc
            Console.WriteLine(e.Error.Message)
            Environment.Exit(ExitCodes.ERROR)
        End If
        Environment.Exit(ExitCodes.OK)
    End Sub

    Private Sub Client_DownloadProgressChanged(sender As Object, e As DownloadProgressChangedEventArgs) Handles Client.DownloadProgressChanged
        Dim Msg As String = "Downloaded " & FormatSize(e.BytesReceived)

        If e.TotalBytesToReceive > 0 Then
            ' Show total if we have it
            Msg &= " of " & FormatSize(e.TotalBytesToReceive)
        End If

        If e.ProgressPercentage > 0 Then
            ' Show percentage if we have it
            Msg &= " (" & e.ProgressPercentage & "%)"
        End If
        Msg &= Space(Console.BufferWidth - Msg.Length) ' Pad to width of screen, so we don't leave any crap

        ' Lock console so other threads won't interfere
        SyncLock My.Application
            Console.CursorLeft = CursorLeft
            Console.CursorTop = CursorTop
            Console.WriteLine(Msg)
        End SyncLock

    End Sub

End Module

''' <summary>
''' Enables FTP over SSL
''' </summary>
Class FtpsWebRequestCreator
    Implements IWebRequestCreate

    Public Function Create(uri As Uri) As WebRequest Implements IWebRequestCreate.Create
        Dim wr As FtpWebRequest = Net.WebRequest.Create(uri.AbsoluteUri.Replace("ftps://", "ftp://"))
        wr.EnableSsl = True
        Return wr
    End Function

End Class